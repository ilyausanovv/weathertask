package weather;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public class WeatherData {
    public static void main(String[] args) throws IOException {
    }

    public void parseOfWeather() throws IOException, FileNotFoundException {

        int verticalLine = 0;
        int horizontalLine = 0;
        float mostHighTemperature = 0;
        float mostLowHumidity = 100;
        float mostHighWind = 0;
        float averageTemp = 0;
        float averageHumidity = 0;
        float averageWind = 0;
        String dayAndHoursWithMostHighTemperature = null;
        String dayAndHoursWithMostLowHumidity = null;
        String dayAndHoursWithMostHighWind = null;
        String mostFrequentDirectionOfWind;
        String string;

        ArrayList<WeatherInformation> weatherInformation = new ArrayList<>();
        FileReader fileReader = new FileReader("C:\\Users\\Ilya\\Desktop\\BaselWeather\\dataexport_20210320T064822.csv");
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while ((string = bufferedReader.readLine()) != null) {
            String[] fields = string.split(",");

            try {
                Date date = new Date();
                String[] parsing = fields[0].split("");

                date.setYear(Integer.parseInt(parsing[0].trim() + parsing[1].trim() + parsing[2].trim() + parsing[3].trim()));
                date.setMonth(Integer.parseInt(parsing[4].trim() + parsing[5].trim()));
                date.setDate(Integer.parseInt(parsing[6].trim() + parsing[7].trim()));
                date.setHours(Integer.parseInt(parsing[9].trim() + parsing[10].trim()));
                float temperature = Float.parseFloat(fields[1]);
                float humidity = Float.parseFloat(fields[2]);
                float speedOfWind = Float.parseFloat(fields[3]);
                float directionOfWind = Float.parseFloat(fields[4]);

                weatherInformation.add(new WeatherInformation(date, temperature, humidity, speedOfWind, directionOfWind));
            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (NumberFormatException e) {

            }
        }

        for (int i = 0; i < weatherInformation.size(); i++) {

            if(weatherInformation.get(i).getTemperature() > 0){
                float sum = 0;
                for (int j = 0; j < weatherInformation.size(); j++) {
                    sum += weatherInformation.get(j).getTemperature();
                }
                averageTemp = sum  / weatherInformation.size();
            }

            if(weatherInformation.get(i).getHumidity() > 0){
                float sum = 0;
                for (int j = 0; j < weatherInformation.size(); j++) {
                    sum += weatherInformation.get(j).getHumidity();
                }
                averageHumidity = sum  / weatherInformation.size();
            }

            if(weatherInformation.get(i).getWindSpeed() > 0){
                float sum = 0;
                for (int j = 0; j < weatherInformation.size(); j++) {
                    sum += weatherInformation.get(j).getWindSpeed();
                }
                averageWind = sum  / weatherInformation.size();
            }

            if (weatherInformation.get(i).getTemperature() > mostHighTemperature) {
                mostHighTemperature = weatherInformation.get(i).getTemperature();
                dayAndHoursWithMostHighTemperature = ("Day and hour with most high temperature: " + weatherInformation.get(i).getDate());
            }

            if (weatherInformation.get(i).getHumidity() < mostLowHumidity) {
                mostLowHumidity = weatherInformation.get(i).getHumidity();
                dayAndHoursWithMostLowHumidity = ("Day and hour with most low humidity: " + weatherInformation.get(i).getDate());
            }

            if (weatherInformation.get(i).getWindSpeed() > mostHighWind) {
                mostHighWind = weatherInformation.get(i).getWindSpeed();
                dayAndHoursWithMostHighWind = ("Day and hour with most strong wind: " + weatherInformation.get(i).getDate());
            }

            if (weatherInformation.get(i).getWindDirection() > 45 && weatherInformation.get(i).getWindDirection() < 135) {
                horizontalLine++;
            }

            if (weatherInformation.get(i).getWindDirection() > 225 && weatherInformation.get(i).getWindDirection() < 315) {
                horizontalLine--;
            }

            if (weatherInformation.get(i).getWindDirection() > 315 && weatherInformation.get(i).getWindDirection() < 45) {
                verticalLine++;
            }

            if (weatherInformation.get(i).getWindDirection() > 135 && weatherInformation.get(i).getWindDirection() < 225) {
                verticalLine--;
            }

        }

        if (Math.abs(horizontalLine) > Math.abs(verticalLine)) {
            if (verticalLine > 0) {
                mostFrequentDirectionOfWind = "North";
            } else {
                mostFrequentDirectionOfWind = "South";
            }

        } else {
            if (horizontalLine > 0) {
                mostFrequentDirectionOfWind = "East";
            } else {
                mostFrequentDirectionOfWind = "West";
            }
        }


        mostFrequentDirectionOfWind = "The most frequent humidity: " + mostFrequentDirectionOfWind;
        File file = new File("C:\\Users\\Ilya\\Desktop\\BaselWeather\\NewInformation.txt");
        file.createNewFile();

        FileWriter fileWriter = new FileWriter(file.getPath());

        fileWriter.write("Average temperature: " + averageTemp + "\n");
        fileWriter.write("Average humidity: " + averageHumidity + "\n");
        fileWriter.write("Average wind: " + averageWind + "\n");
        fileWriter.write(dayAndHoursWithMostHighTemperature + "\n");
        fileWriter.write(dayAndHoursWithMostHighWind + "\n");
        fileWriter.write(dayAndHoursWithMostLowHumidity + "\n");
        fileWriter.write(mostFrequentDirectionOfWind);

        fileWriter.close();


    }

}
